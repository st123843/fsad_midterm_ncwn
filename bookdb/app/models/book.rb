class Book < ApplicationRecord
    validates :bookname, presence: true
    validates :author, presence: true

    has_many :readers
  end
  