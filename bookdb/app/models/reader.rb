class Reader < ApplicationRecord
  belongs_to :book
  validates :readername, presence: true
  validates :comments, length: { maximum: 20 }
  validates :rating, presence: true, numericality: { greater_than: 0, less_than_or_equal_to: 5 }
end